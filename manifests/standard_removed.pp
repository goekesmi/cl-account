define account::standard_removed (
	$username = $title,
	$id,
	$comment = '',
	$key = 'NONE',
	$dsskey = 'NONE',
	$shell = '/bin/sh',
	$password = '*',
	$groups = [], 
	) {
	require account

	$ensure = 'present'

	$sudogroup = $::operatingsystem ? {
		'CentOS' 	=> ['wheel'], 
		'freebsd' 	=> ['wheel'], 
		'Debian' 	=> ['sudo'], 
		}

#	$applygroups = split(inline_template("<%= (default_env).join(',') %>"),',')
		
	user {"$username":
		ensure  => $ensure,
		uid => $id,
		gid => $id,
		groups => [$sudogroup ],
		home => "/home/$username",
		require => Group["$username"],
		comment => $comment,
		password => '*',
	    }

	group {"$username":
		ensure => $ensure,
		gid => $id,
		}


	ssh_authorized_key {"puppet managed rsa for $username":
		user => $username,
		ensure => absent,
		type => "ssh-rsa",
		key => $key,
		}
	ssh_authorized_key {"puppet managed dss for $username":
		user => $username,
		ensure => absent,
		type => "ssh-dss",
		key => $dsskey,
		}

	}

