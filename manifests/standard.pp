define account::standard (
	$username = $title,
	$id,
	$comment = '',
	$key = 'NONE',
	$dsskey = 'NONE',
	$shell,
	$password = '*',
	$groups = [], 
	) {
	require account
	require shells

	$ensure = 'present'

	$sudogroup = $::operatingsystem ? {
		'CentOS' 	=> ['wheel'], 
		'freebsd' 	=> ['wheel'], 
		'Debian' 	=> ['sudo'], 
		}

	user {"$username":
		ensure  => $ensure,
		uid => $id,
		gid => $id,
		groups => [$sudogroup ],
		home => "/home/$username",
		require => [Group["$username"],  ],
		shell => "$shell",
		comment => $comment,
		password => $password,
	    }

	realize Exec["/usr/bin/grep ^$shell$ /etc/shells"]



	group {"$username":
		ensure => $ensure,
		gid => $id,
		}

	file {"/home/$username/":
		ensure => directory,
		owner => $username,
		group => $username,
		require => [ User["$username"], 
				File['/home/'], 
				Zfs["dpool/home/$username"] 
				],
		
		}

	file {"/home/$username/.ssh/":
		ensure => directory,
		owner => $username,
		group => $username,
		require => [ User["$username"], 
				File["/home/$username/"], 
				],
		
		}
	file {"/home/$username/.ssh/authorized_keys":
		ensure =>present, 
		owner => $username,
		group => $username,
		mode => 'u+rw',
		require => [ User["$username"], 
				File["/home/$username/.ssh/"], 
				],
		}

	ssh_authorized_key {"puppet managed rsa for $username":
		require => [ File["/home/$username/"], File["/home/$username/.ssh/authorized_keys"], ],
		user => $username,
		ensure => present,
		type => "ssh-rsa",
		key => $key,
		}
	ssh_authorized_key {"puppet managed dss for $username":
		require => [ File["/home/$username/"], File["/home/$username/.ssh/authorized_keys"], ],
		user => $username,
		ensure => present,
		type => "ssh-dss",
		key => $dsskey,
		}

	zfs { "$::home_zpool/home/$username":
		ensure => present,
		}

	# Enforce Unique account ID's on a given system for standard accounts.

	unique{"$id": }

	}

