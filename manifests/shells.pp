# This class exists to list a series of detectors to make sure that
# nobody configures a shell that doesn't exist.  The standard 
# account attempts to realize an exec line like this for each
# listed shell.  It will attempt to verify the shell is listed.
# if it isn't the manifest fails to install.
class account::shells (
	) {

	@exec { "/usr/bin/grep ^/bin/sh$ /etc/shells": }
	@exec { "/usr/bin/grep ^/bin/csh$ /etc/shells": }
	@exec { "/usr/bin/grep ^/bin/tcsh$ /etc/shells": }
	@exec { "/usr/bin/grep ^/bin/bash$ /etc/shells": }
	@exec { "/usr/bin/grep ^/usr/local/bin/bash$ /etc/shells": }
	@exec { "/usr/bin/grep ^/bin/false$ /etc/shells": }
	

	}

