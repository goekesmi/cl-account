define account::root ( 
	$username = $title, 
	$id = "",
	$key = "NONERSA", 
	$dsskey = "NONEDSS", 
	$comment = "", 
	$shell = '/bin/sh',
	$password ='*',
	) {
	require account
	user {"$username":
		ensure  => present,
		allowdupe => true,
		uid => 0,
		gid => 0,
		home => "/Rusr/$username/",
		comment => $comment,
		password => $password,
	    }


	file {"/Rusr/$username/":
		ensure => directory,
		owner => "$username",
		group => 0,
		require => [User["$username"], File["/Rusr"]],
		}


	ssh_authorized_key {"account managed rsa for $username":
		require =>  File["/Rusr/$username/"], 
		user => "root",
		ensure => present,
		type => "ssh-rsa",
		key => $key,
		target => "/Rusr/$username/.ssh/authorized_keys",
		}

	ssh_authorized_key {"account managed dss for $username":
		require =>  File["/Rusr/$username/"],
		user => "root",
		ensure => present,
		type => "ssh-dss",
		key => $dsskey,
		target => "/Rusr/$username/.ssh/authorized_keys",
		}
        }

