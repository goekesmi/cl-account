Account managment Computer Lab style.  Manages root and non-root
accounts with ssh keys, ownership.  Works on FreeBSD.  I hope to 
make it multiplatform eventually, but those bits died from disuse.

Example invocations.

	account::standard { 'username':
		id      => 1001,
		key => "RSA KEY MATERIAL AAAAB3NzaC1ycbqck=",
		password => 'PASSWORD SHA1 HASH IQggAg5bQEqs1NGG0',
		comment => 'User Name',
		shell => '/bin/zsh',
		}
	account::root { 'Runn':
		key => "RSA KEY MATERIAL AAAAB3NzapgnFLfHnzHdNMJLoxqRbqck=",
		}


Eastablishes /home and /Rusr trees.  Maintains keys for ssh and passwords.
Generally has safe, if minimally functional defaults.

