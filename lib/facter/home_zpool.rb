# home_zpool.rb

Facter.add("home_zpool") do
  setcode do
    Facter::Util::Resolution.exec("zfs list -H | grep /home$ | cut -f 1 | cut -f 1 -d '/'")
  end
end
